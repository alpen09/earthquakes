//
//  Constants.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 05/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

let north: Float = 44.1 // 44.1
let south: Float = -9.9 // -9.9
let east: Float = -22.4 // -22.4
let west: Float = 55.2 // 55.2
let maxRows: Int = 100 // 10
