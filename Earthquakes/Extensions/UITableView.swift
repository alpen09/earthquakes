//
//  UITableView.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueReusable<T: UITableViewCell>(cellIdentifier: String, indexPath: IndexPath) -> T {
        guard let tableCell = self.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? T else {
            let errorMessage = "Variable tableCell of type UITableViewCell cannot be casted to \(type(of: T.self))"
            preconditionFailure(errorMessage)
        }

        return tableCell
    }

}
