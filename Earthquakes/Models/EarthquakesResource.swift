//
//  EarthquakesResource.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

struct EarthquakesResource: Codable {
    var earthquakes: [Earthquake]?
}
