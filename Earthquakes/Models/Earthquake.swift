//
//  Earthquake.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

struct Earthquake: Codable {
    var eqid: String?
    var datetime: Date?
    var depth: Float?
    var lng: Float?
    var lat: Float?
    var src: String?
    var magnitude: Float?
}
