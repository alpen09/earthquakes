//
//  StoryboardProtocol.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit

/// Used to identify a storyboard
protocol StoryboardProtocol {
    static var name: String { get }
    var vcIdentifier: String { get }
}

extension StoryboardProtocol {
    func get<T: UIViewController>() -> T {
        let stboard = UIStoryboard(name: Self.name, bundle: nil)
        guard let viewController = stboard.instantiateViewController(withIdentifier: self.vcIdentifier) as? T else {
            preconditionFailure()
        }
        return viewController
    }
}

enum StoryboardMain: StoryboardProtocol {
    static let name = "Main"

    case mapViewController

    var vcIdentifier: String {
        switch self {
        case .mapViewController:
            return "MapViewController"
        }
    }
}
