//
//  ViewController.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var earthquakes: UITableView!

    private var earthquakesSource: [Earthquake] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        earthquakes.register(UINib(nibName: String(describing: EarthquakeCell.self),
                                   bundle: nil),
        forCellReuseIdentifier: String(describing: EarthquakeCell.self))
        earthquakes.estimatedRowHeight = 103
        earthquakes.refreshControl = UIRefreshControl()
        earthquakes.refreshControl?.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        callEarthquakes()
    }

    private func callEarthquakes() {
        self.earthquakes.refreshControl?.beginRefreshing()
        EarthquakeService().getEarthquakes { earthquakesResponse, error in
            if let _error = error {
                self.earthquakes.refreshControl?.endRefreshing()
                _error.showSimpleError(viewController: self)
                return
            }
            self.earthquakesSource = earthquakesResponse
            self.earthquakes.reloadData()
            self.earthquakes.refreshControl?.endRefreshing()
            if earthquakesResponse.count == 0 {
                APIError.init(message: "No earthquakes were found for the given parameters",
                              error: nil,
                              data: nil,
                              httpStatusCode: nil).showSimpleError(viewController: self)
            }
        }
    }

    @objc private func refresh(_ sender: AnyObject) {
       callEarthquakes()
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        earthquakesSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let earthquake = earthquakesSource[indexPath.row]
        let cell: EarthquakeCell = tableView
            .dequeueReusable(cellIdentifier: String(describing: EarthquakeCell.self),
                             indexPath: indexPath)
        cell.configure(earthquake: earthquake)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let earthquake = earthquakesSource[indexPath.row]
        let mapViewController: MapViewController = StoryboardMain.mapViewController.get()
        mapViewController.configure(earthquake: earthquake)
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }

}
