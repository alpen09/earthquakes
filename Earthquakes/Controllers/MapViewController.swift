//
//  MapViewController.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    private var earthquake: Earthquake?
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\(earthquake?.eqid ?? "")"
        guard let lat = earthquake?.lat, let long = earthquake?.lng else {
            return
        }
        setPinUsingMKPointAnnotation(location: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat),
                                                                      longitude: CLLocationDegrees(long)))
    }

    private func setPinUsingMKPointAnnotation(location: CLLocationCoordinate2D){
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Magnitude \(earthquake?.magnitude ?? 0.0)"
        annotation.subtitle = "Depth: \(earthquake?.depth ?? 0.0) km"
        let coordinateRegion = MKCoordinateRegion(center: annotation.coordinate,
                                                  latitudinalMeters: 200000,
                                                  longitudinalMeters: 200000)
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.addAnnotation(annotation)
    }

    func configure(earthquake: Earthquake) {
        self.earthquake = earthquake
    }

}
