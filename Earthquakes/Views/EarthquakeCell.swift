//
//  EarthquakeCell.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit

class EarthquakeCell: UITableViewCell {
    @IBOutlet private weak var dateDesc: UILabel!
    @IBOutlet private weak var magnitudeDesc: UILabel!
    @IBOutlet private weak var flag: UILabel!
    @IBOutlet private weak var dateValue: UILabel!
    @IBOutlet private weak var magnitudeValue: UILabel!
    @IBOutlet private weak var depthValue: UILabel!
    @IBOutlet weak var idValue: UILabel!
    
    func configure(earthquake: Earthquake) {
        dateValue.text = EarthquakeCellHelper.safeValue(date: earthquake.datetime)
        magnitudeValue.text = EarthquakeCellHelper.safeValue(float: earthquake.magnitude)
        depthValue.text = EarthquakeCellHelper.safeValue(float: earthquake.depth)
        flag.text = EarthquakeCellHelper.flag(country: earthquake.src)
        idValue.text = EarthquakeCellHelper.safeValue(string: earthquake.eqid)

        if let mMagnitude = earthquake.magnitude, mMagnitude >= 8.0 {
            magnitudeDesc.textColor = UIColor.systemRed
            magnitudeValue.textColor = UIColor.systemRed
        } else {
            magnitudeDesc.textColor = UIColor.label
            magnitudeValue.textColor = UIColor.label
        }

        self.isAccessibilityElement = true
    }

}
