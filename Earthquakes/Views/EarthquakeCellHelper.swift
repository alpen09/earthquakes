//
//  EarthquakeCellHelper.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit

class EarthquakeCellHelper {

    static let noValueAvailable = "No value available"

    static func safeValue(string: String?) -> String {
        guard let result = string, !result.isEmpty else {
            return noValueAvailable
        }
        return result
    }

    static func safeValue(float: Float?) -> String {
        guard let result = float, result >= 0.0 else {
            return noValueAvailable
        }

        return String(result)
    }

    static func safeValue(date: Date?) -> String {
        guard let result = date else {
            return noValueAvailable
        }

        let myString = EarthquakeService.dateFormatter.string(from: result)
        let friendlyDate = EarthquakeService.dateFormatter.date(from: myString)
        let formatter = DateFormatter()
        formatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
        if let mFriendlyDate = friendlyDate {
            return formatter.string(from: mFriendlyDate)
        }


        return noValueAvailable
    }

    static func flag(country: String?) -> String {
        let mCountry = validateCountryCode(country: country)
        guard !mCountry.isEmpty else {
            return ""
        }
        let base : UInt32 = 127397
        var result = ""
        for unicode in mCountry.unicodeScalars {
            if let unicodeScalar = UnicodeScalar(base + unicode.value) {
                result.unicodeScalars.append(unicodeScalar)
            }
        }
        return String(result)
    }

    static func validateCountryCode(country: String?) -> String {
        guard var mCountry = country else {
            return ""
        }
        mCountry = mCountry.uppercased()
        guard NSLocale.isoCountryCodes.contains(mCountry) else {
            return ""
        }
        return mCountry
    }

}
