//
//  APIResponse.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 02/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

class APIResponse {
    var success: Bool
    var data: Data?
    var error: APIError?

    init(success: Bool, error: APIError?, data: Data?) {
        self.success = success
        self.error = error
        self.data = data
    }

}
