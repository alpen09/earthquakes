//
//  APIClient.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

class APIClient {

    public static let sharedInstance = APIClient()

    typealias ResponseHandler = (APIResponse) -> Void

    func callAPI(apiRequest: APIRequest, completionHandler: @escaping ResponseHandler) {
        guard let url = buildURL(apiRequest: apiRequest) else {
            let mError = APIError.init(message: "An error has occured while parsing the url.", error: nil, data: nil, httpStatusCode: nil)
            completionHandler(APIResponse.init(success: false, error: mError, data: nil))
            return
        }

        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                let mError = APIError.init(message: nil, error: error as NSError, data: nil, httpStatusCode: nil)
                completionHandler(APIResponse.init(success: false, error: mError, data: data))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    let mError = APIError.init(message: nil, error: nil, data: data, httpStatusCode: (response as? HTTPURLResponse)?.statusCode)
                    completionHandler(APIResponse.init(success: false, error: mError, data: data))
                    return
            }
            completionHandler(APIResponse.init(success: true, error: nil, data: data))
        })
        task.resume()
    }

    func buildURL(apiRequest: APIRequest) -> URL? {
        guard var urlComponents = URLComponents
            .init(string: "\(apiRequest.domainUrlString)\(apiRequest.apiVersion)\(apiRequest.apiPath)") else {
                return nil
        }
        if let params = apiRequest.params {
            urlComponents.queryItems = convertToQueryItems(params: params)
        }
        return urlComponents.url
    }

    private func convertToQueryItems(params: [String: String]) -> [URLQueryItem]? {
        return params.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
