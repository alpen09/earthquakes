//
//  ErrorResource.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 02/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

struct ErrorResource: Codable {

    struct Error: Codable {
        let message: String?
        let value: Int?
    }

    let status: Error?
}
