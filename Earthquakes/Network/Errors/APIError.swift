//
//  APIError.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 02/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import UIKit

class APIError {
    var message: String?
    var data: Data?
    var error: NSError?
    var httpStatusCode: Int?

    let defaultErrorTitle = "Oops! Apologies!"
    let defaultErrorButtonTitle = "OK"

    init(message: String?, error: NSError?, data: Data?, httpStatusCode: Int?) {
        self.message = message
        self.error = error
        self.data = data
        self.httpStatusCode = httpStatusCode
    }

    func showSimpleError(viewController: UIViewController) {
        if let _message = message, !_message.isEmpty {
            showError(viewController: viewController,
                      title: defaultErrorTitle,
                      message: _message,
                      buttonTitle: defaultErrorButtonTitle)
        } else if let _error = error {
            showError(viewController: viewController,
                      title: defaultErrorTitle,
                      message: _error.localizedDescription,
                      buttonTitle: defaultErrorButtonTitle)
        } else if let resultMessage = parseErrorMessage(data: data) {
            var messageToShow = resultMessage
            if let _httpStatusCode = httpStatusCode {
                messageToShow = "\(resultMessage) \nStatus code: \(_httpStatusCode)"
            }
            showError(viewController: viewController,
                      title: defaultErrorTitle,
                      message: messageToShow,
                      buttonTitle: defaultErrorButtonTitle)
        } else {
            showUnexpectedError(viewController: viewController)
        }
    }

    private func parseErrorMessage(data: Data?) -> String? {
        if let _data = data, let errorRes = try? JSONDecoder().decode(ErrorResource.self, from: _data),
            let resultError = errorRes.status,
            let resultMessage = resultError.message,
            !resultMessage.isEmpty {
            return resultMessage
        }
        return nil
    }

    func showUnexpectedError(viewController: UIViewController) {
        showError(viewController: viewController,
                  title: defaultErrorTitle,
                  message: "It seems that an unexpected error has occured.",
                  buttonTitle: defaultErrorButtonTitle)
    }

    private func showError(viewController: UIViewController,
                           title: String,
                           message: String,
                           buttonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }

}
