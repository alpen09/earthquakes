//
//  EarthquakeService.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 02/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

class EarthquakeService {

    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()

    func getEarthquakes(north: Float = north,
                        south: Float = south,
                        east: Float = east,
                        west: Float = west,
                        maxRows: Int = maxRows,
                        completionHandler: @escaping ([Earthquake], APIError?) -> Void) {
        let params: [String: String] = ["formatted": "true",
                                        "north": "\(north)",
                                        "south": "\(south)",
                                        "east": "\(east)",
                                        "west": "\(west)",
                                        "maxRows": "\(maxRows)",
                                        "username": "mkoppelman"]
        let apiRequst = APIRequest.init(apiVersion: "",
                                        apiPath: "earthquakesJSON",
                                        params: params)
        APIClient.sharedInstance.callAPI(apiRequest: apiRequst) { response in
            if response.success {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .formatted(EarthquakeService.dateFormatter)
                if let data = response.data,
                    let earthquakesRes = try? jsonDecoder.decode(EarthquakesResource.self, from: data),
                    let earthquakes = earthquakesRes.earthquakes {
                    DispatchQueue.main.async {
                        completionHandler(earthquakes, nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        completionHandler([], response.error)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler([], response.error)
                }
            }
        }
    }
}
