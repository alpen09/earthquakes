//
//  APIRequest.swift
//  Earthquakes
//
//  Created by Aleksandar Penev on 02/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import Foundation

class APIRequest {
    let domainUrlString: String = {
        #if DEBUG || TESTING || STAGING
            return "http://api.geonames.org/" // staging url
        #else
            return "http://api.geonames.org/" // production url
        #endif
    }()
    var apiVersion: String
    var apiPath: String
    var params: [String: String]?

    init(apiVersion: String, apiPath: String, params: [String: String]?) {
        self.apiVersion = apiVersion
        self.apiPath = apiPath
        self.params = params
    }

}
