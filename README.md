# Test-Earthquake

This is the Earthquakes project. 

 - The app loads a limited to 10 entries list of earthquakes, gives information about them and pins them on a map.
 You can change the limit in the Constants.swift file following the API documentation here http://www.geonames.org/export/JSON-webservices.html#earthquakesJSON

 - It's fully built in Swift using the MVC pattern with a Network extension. 

 - The app is currently supporting iOS versions from 13.0 and above. This is because of the new SceneDelegate approach of the AppDelegate area. 

 - The project includes unit and UI tests. System colours are used intentionally, so the app would be "dark mode" compatible. 

 - The project is built on Xcode 11.3.1 on a macOS Mojave and Xcode 11.6 on macOS Catalina.
