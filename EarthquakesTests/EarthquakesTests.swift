//
//  EarthquakesTests.swift
//  EarthquakesTests
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import XCTest
@testable import Earthquakes

class EarthquakesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSafeValueString() {
        XCTAssertEqual(EarthquakeCellHelper.safeValue(string: nil),
                       "No value available",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.safeValue(string: ""),
                       "No value available",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.safeValue(string: "test"),
                       "test",
                       "safe value is wrong")
    }

    func testSafeValueFloat() {
        XCTAssertEqual(EarthquakeCellHelper.safeValue(float: nil),
                       "No value available",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.safeValue(float: -10.12736),
                       "No value available",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.safeValue(float: 0.0),
                       "0.0",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.safeValue(float: 8.0),
                       "8.0",
                       "safe value is wrong")
    }

    func testSafeValueDate() {
        XCTAssertEqual(EarthquakeCellHelper.safeValue(date: nil),
                       "No value available",
                       "safe value is wrong")

        let formatter = DateFormatter()
        formatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
        let date = Date()
        XCTAssertEqual(EarthquakeCellHelper.safeValue(date: date),
                       formatter.string(from: date),
                       "safe value is wrong")
    }

    func testCountryCodeValidation() {
        XCTAssertEqual(EarthquakeCellHelper.validateCountryCode(country: nil),
                       "",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.validateCountryCode(country: ""),
                       "",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.validateCountryCode(country: "us"),
                       "US",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.validateCountryCode(country: "at"),
                       "AT",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.validateCountryCode(country: "test"),
                       "",
                       "safe value is wrong")
    }

    func testCountryCodeEmoji() {
        XCTAssertEqual(EarthquakeCellHelper.flag(country: nil),
                       "",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.flag(country: ""),
                       "",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.flag(country: "us"),
                       "🇺🇸",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.flag(country: "at"),
                       "🇦🇹",
                       "safe value is wrong")

        XCTAssertEqual(EarthquakeCellHelper.flag(country: "test"),
                       "",
                       "safe value is wrong")
    }

    func testStoryboardProtocol() {
        XCTAssertEqual(String(describing: type(of: StoryboardMain.mapViewController.get())),
                       String(describing: type(of: MapViewController.init())),
                       "wrong controller")
    }

    func testbuildURL() {
        var apiRequest = APIRequest.init(apiVersion: "", apiPath: "", params: nil)
        XCTAssertEqual(APIClient.sharedInstance.buildURL(apiRequest: apiRequest),
                       URL.init(string: apiRequest.domainUrlString)!,
                       "wrong api request")
        apiRequest = APIRequest.init(apiVersion: "v2/", apiPath: "test/", params: nil)
        XCTAssertEqual(APIClient.sharedInstance.buildURL(apiRequest: apiRequest),
                       URL.init(string: apiRequest.domainUrlString + apiRequest.apiVersion + apiRequest.apiPath)!,
                       "wrong api request")
        apiRequest = APIRequest.init(apiVersion: "v2", apiPath: "test", params: nil)
        XCTAssertEqual(APIClient.sharedInstance.buildURL(apiRequest: apiRequest),
                       URL.init(string: apiRequest.domainUrlString + apiRequest.apiVersion + apiRequest.apiPath)!,
                       "wrong api request")
        apiRequest = APIRequest.init(apiVersion: "*ß", apiPath: "´´&", params: nil)
        XCTAssertEqual(APIClient.sharedInstance.buildURL(apiRequest: apiRequest)?.absoluteString,
                       nil,
                       "wrong api request")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
