//
//  EarthquakesUITests.swift
//  EarthquakesUITests
//
//  Created by Aleksandar Penev on 01/08/2020.
//  Copyright © 2020 al penev. All rights reserved.
//

import XCTest

class EarthquakesUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        app = XCUIApplication()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        if let failureCount = testRun?.failureCount, failureCount > 0 {
            let screenshot = XCUIScreen.main.screenshot()
            let attach = XCTAttachment(screenshot: screenshot)
            add(attach)
        }
        super.tearDown()
    }

    func testEarthquakes() {
        // UI tests must launch the application that they test.
        app.launch()

        doesTableHaveCells()

        checkCellsElementIfEmptyString(identifier: "date_value")
        checkCellsElementIfEmptyString(identifier: "magnitude_value")
        checkCellsElementIfEmptyString(identifier: "depth_value")
        checkCellsElementIfEmptyString(identifier: "flag_value")
        checkCellsElementIfEmptyString(identifier: "id_value")

        let magnitudeDesc = app.tables.cells.firstMatch.staticTexts.element(matching: .any, identifier: "magnitude")
        let magnitudeValue = app.tables.cells.firstMatch.staticTexts.element(matching: .any, identifier: "magnitude_value")
        let annotationString = "\(magnitudeDesc.label) \(magnitudeValue.label)"

        app.tables.cells.firstMatch.tap()

        sleep(3)

        XCTAssert(app.maps.firstMatch.exists)

        app.maps.containing(.other, identifier: annotationString).element.tap()

        sleep(5)

        app.navigationBars.firstMatch.buttons.firstMatch.tap()
    }

    func doesTableHaveCells() {
        let countGreatThanZero = NSPredicate(format: "count > 0")
        expectation(for: countGreatThanZero, evaluatedWith: app.tables.cells, handler: nil)
        waitForExpectations(timeout: 10.0, handler: nil)
    }

    func checkCellsElementIfEmptyString(identifier: String) {
        let value = app.tables.cells.firstMatch.staticTexts.element(matching: .any, identifier: identifier)
        XCTAssert(!value.label.isEmpty)
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
